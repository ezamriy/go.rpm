%global pkg               go
%global debug_package     %{nil}
%global __os_install_post %{nil}

Name:           go
Version:        1.1.2
Release:        1%{?dist}
Summary:        Go programming language compiler and packages

Packager:       Eugene Zamriy <eugene@zamriy.info>

Group:          Development/Languages
License:        BSD
URL:            http://golang.org/
Source0:        http://go.googlecode.com/files/%{pkg}%{version}.src.tar.gz

%if 0%{?fedora}
BuildRequires:  hostname
%endif

ExclusiveArch:  %{ix86} x86_64 arm
%ifarch %{ix86}
    %global GOARCH 386
    %global TOOL_PREFIX 8
%endif
%ifarch x86_64
    %global GOARCH amd64
    %global TOOL_PREFIX 6
%endif
%ifarch %{arm}
    %global GOARCH arm
    %global TOOL_PREFIX 5
%endif

%global GOROOT_FINAL %{_libdir}/go


%description
Go is an open source programming environment that makes it easy to build simple,
reliable, and efficient software.


%package -n emacs-%{pkg}-mode
Summary:        Go mode for Emacs.
Group:          Applications/Editors
BuildArch:      noarch
BuildRequires:  emacs
Requires:       emacs(bin) >= %{_emacs_version}


%description -n emacs-%{pkg}-mode
An Emacs mode for editing Go source code.


%prep
%setup -q -n %{pkg}


%build
GOROOT="$(pwd)"
GOROOT_FINAL="%{GOROOT_FINAL}"
GOBIN="$GOROOT/bin"
GOOS=linux
GOARCH="%{GOARCH}"
export GOROOT GOROOT_FINAL GOBIN GOOS GOARCH

pushd misc/emacs
emacs --batch --eval '(setq load-path (cons "." load-path))' -f batch-byte-compile *.el
popd

mkdir -p "$GOBIN"
cd src

./all.bash


%install
rm -rf %{buildroot}

GOROOT="%{buildroot}%{_libdir}/go"
export GOROOT

mkdir -p $GOROOT/{misc,lib,src}
mkdir -p %{buildroot}%{_bindir}/

cp -ar pkg include lib bin doc favicon.ico robots.txt $GOROOT
cp -ar src/pkg src/cmd $GOROOT/src
cp -ar misc/cgo $GOROOT/misc

%{__install} -pm 755 -d %{buildroot}%{_emacs_sitelispdir}/%{pkg}-mode/
%{__install} -pm 644 misc/emacs/*.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}-mode/
%{__install} -pm 644 misc/emacs/*.elc %{buildroot}%{_emacs_sitelispdir}/%{pkg}-mode/

for cmd in go godoc gofmt; do
ln -sf %{_libdir}/go/bin/$cmd %{buildroot}%{_bindir}/$cmd
done

for cmd in a c g l; do
    ln -sf %{_libdir}/go/pkg/tool/linux_%{GOARCH}/%{TOOL_PREFIX}$cmd \
        %{buildroot}%{_bindir}/%{TOOL_PREFIX}$cmd
done


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS CONTRIBUTORS LICENSE PATENTS README VERSION
%{_bindir}/go*
%{_bindir}/%{TOOL_PREFIX}*
%{_libdir}/go


%files -n emacs-%{pkg}-mode
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}-mode/*.el
%{_emacs_sitelispdir}/%{pkg}-mode/*.elc


%changelog
* Fri Aug 23 2013 Eugene G. Zamriy <eugene@zamriy.info> - 1.1.2-1
- Initial release. 1.1.2 version
